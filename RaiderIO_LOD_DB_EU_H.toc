## Interface: 80205
## Author: cqwrteur
## Title: Raider.IO LOD Database EU Horde
## Version: @project-version@
## LoadOnDemand: 1
## X-RAIDER-IO-LOD: 3
## X-RAIDER-IO-LOD-FACTION: Horde

db/db_eu_horde_characters.lua
db/db_eu_horde_lookup.lua
